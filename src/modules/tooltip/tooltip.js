$(function () {
	'use strict';

	window.UI = window.UI || {};
	
	window.UI.tooltip = {
		init: function() {
			var inst = this;

			$(document).on('click', '.js-tooltip', function(e){
				inst.open(e);
			});
			$(document).on('click', '.js-tooltip-close', function(){
				inst.close();
			});
		},
		open: function(e){
			var jThis = $(e.currentTarget);
			var parent = jThis.parent();
			var item = parent.find('.js-tooltip-item');

			$('.js-tooltip-item').hide();
			item.slideDown(250);
		},
		close: function() {
			$('.js-tooltip-item').hide();
		}
	};

	window.UI.tooltip.init();
}());
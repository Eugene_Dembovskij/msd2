$(function () {
	'use strict';

	window.UI = window.UI || {};
	
	window.UI.modal = {
		cTimer: [],
		init: function() {
			var inst = this;

			$(document).on('click', '.js-modal-btn', function(e){
				clearTimeout(window.UI.modal.cTimer);
				window.UI.modal.cTimer = setTimeout(
					function () {
						inst.open(e);
					},250, e);

			});
			$(document).on('click', '.js-modal__close', function(){
				inst.close();
			});
		},
		open: function(e){
			var jThis = $(e.currentTarget);
			var target = jThis.data('target');
			var modal = $('.js-modal');
			var item = modal.find('[data-item=' + target + ']');

			window.UI.modal.onOpenModal(modal, item);
		},
		close: function() {
			$('body').removeClass('overflow');
			$('body').removeClass('is-modal');
			$('body').css({'padding-right': '0'});
			$('.js-modal').removeClass('is-active');
			$('.js-modal__item').hide();
		},
		closeItems: function() { // -закрываем только итемы модалок
			$('.js-modal__item').hide();
		},
		openFromCode: function(idModal){ // открываем программно модалку по её ID
			var target = idModal;
			var modal = $('.js-modal');
			var item = modal.find('[data-item=' + target + ']');

			window.UI.modal.onOpenModal(modal, item);
		},
		onOpenModal: function(modal, item){ // показываем
			if( modal.get(0) && item.get(0)){
				$('body').addClass('overflow');
				$('body').css({'padding-right': window.UI.scrollbarWidth() + 'px'});
				modal.addClass('is-active');
				$('body').addClass('is-modal');
				if(modal.find('.js-modal__item').is(":visible")){
					window.UI.modal.closeItems();
				}
				item.fadeIn(250);
			}
		}
	};

	window.UI.modal.init();
	// window.UI.modal.openFromCode('faq-01');
}());
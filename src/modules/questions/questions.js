$(function () {
	'use strict';

	window.UI = window.UI || {};
	
	window.UI.quest = {
		rightAnswers: 0,
		init: function() {
			$(document).on('click', '.js-questions-next', this.next);
			$(document).on('click', '.js-questions-choose', this.choose);
			$(document).on('click', '.js-questions-reset', this.reset);
		},
		choose: function(){
			var answer = $(this);
			var help = answer.find('.answer__help');
			var question = answer.parent();
			var rightAnswer = question.find('[data-answer*="true"]');

			if (!question.hasClass('choosen')) {
				help.slideDown(250);
				question.addClass('choosen');
				$('.questions__btn').addClass('active');
				rightAnswer.addClass('lighted');
				
				if (answer.data('answer') == true) {
					window.UI.quest.rightAnswers += 1;
				}
			}
		},
		next: function(){
			var btn = $(this).parent();
			var questionText = $('.questions__title');
			var questionsItem = $('.questions__item');
			var activeQuestion = $('.questions__item.active').index();
			var nextQuestion = questionsItem[activeQuestion + 1];
			var nextQuestionData = $(nextQuestion).data('question');
			var counter = $('.questions__counter');

			if (activeQuestion + 1 <= questionsItem.length) {
				counter.text(activeQuestion + 2 + '/' + questionsItem.length);
				questionsItem.removeClass('active');
				$(nextQuestion).addClass('active');
				questionText.text(nextQuestionData);
				btn.removeClass('active');

				$('html, body').stop().animate({
					'scrollTop': (counter[0].offsetTop)
				}, 750);

				if (activeQuestion + 1 == questionsItem.length - 1) {
					$(this).text('Результаты теста');
				}

				if (activeQuestion + 1 == questionsItem.length) {
					$('.questions').hide();
					$('.test-banner').show();
					console.log(window.UI.quest.rightAnswers);
					$('.js-questions-result').text(window.UI.quest.rightAnswers);
				}
			}
		},
		reset: function() {
			var qItems = $('.questions__item');

			window.UI.quest.rightAnswers = 0;
			$('.test-banner').hide();
			$('.questions').show();
			qItems.removeClass('active choosen');
			$(qItems[0]).addClass('active');
			$('.answer').removeClass('lighted');
			$('.answer__help').hide();

			$('.questions__counter').text('1/' + qItems.length);
		}
	};

	window.UI.quest.init();
}());
'use strict'

$(window).bind("load", function() {
	$('.js-main-section').on('wheel', function(event){
		var mainSlider = MainSlider.el;
		var currentSlide = mainSlider[0].slick.currentSlide;
		var count = mainSlider[0].slick.slideCount - 1;
		var scrollPos = $(window).scrollTop();
		
		if (scrollPos === 0) {
			if (event.originalEvent.deltaY < 0 && currentSlide > 0) {
				mainSlider.slick('slickPrev');
			} else if (event.originalEvent.deltaY > 0) {
				event.preventDefault();
				event.stopPropagation();

				if (currentSlide === count) {
					var nextSection =  $('#anchor-eff');

					$('html, body').stop().animate({
						'scrollTop': nextSection.offset().top
					}, 750);
				}
				mainSlider.slick('slickNext');
			}
		}
	});
});

window.UI = window.UI || {};
	
window.UI.moveToAnchor = {
	init: function() {
		var inst = this;

		$(document).on('click', '.js-anchor', function(e){
			inst.onClick(e);
		});
	},
	onClick: function(e){
		e.preventDefault();
		
		var jThis = $(e.currentTarget);
		var	jTarget = ( jThis.data("target").indexOf(".") == -1 ) ? $("#" + jThis.data("target")) : $(jThis.data("target"));
		
		if( jTarget.length ){
			// if (window.UI.menu.isOpen) {
			// 	window.UI.menu.close();
			// }
			
			$('html, body').stop().animate({
				'scrollTop': jTarget.offset().top
			}, 750);
		}
	}	
};

window.UI.moveToAnchor.init();


window.UI.scrollbarWidth = function() {
	var block = $('<div>').css({'height':'50px','width':'50px'}),
		indicator = $('<div>').css({'height':'200px'});

		$('body').append(block.append(indicator));
		var w1 = $('div', block).innerWidth();
		block.css('overflow-y', 'scroll');
		var w2 = $('div', block).innerWidth();
		$(block).remove();
		return (w1 - w2);
};

var ProductSlider = {
	parentEl: '.js-product-use',
	slider: null,
	sliderOpts: {
		nextArrow: '<span class="slick-arrow--next"></span>',
		prevArrow: '<span class="slick-arrow--prev"></span>',
		infinite: true,
		dots: true,
		arrows: true,
		centerMode: false,
		slidesToShow: 1,
		dotsClass: 'product-use__dots slick-dots',
		// appendArrows: '.js-gerber-pup-slider .rg-b-gerber__pUps-items-nav-box',
		// appendDots: '.js-gerber-pup-slider .rg-b-gerber__pUps-items-dots-box',
		slidesToScroll: 1
	},
	init: function() {
		var oThis = this;
		if ($(oThis.parentEl + ':visible').length) {
			$(oThis.parentEl + ':visible').slick(oThis.sliderOpts);
		}
	}
};

var faqSlider = {
	box: [],
	slider: [],
	init: function () {
		faqSlider.box = $('.js-faq-slider');
		if (faqSlider.box){
			faqSlider.slider = faqSlider.box.find('.js-faq-slider__list');
			if (faqSlider.slider){
				faqSlider.initSlider();
			}
		}
	},
	initSlider: function () {
		faqSlider.slider.slick({
			dots: false,
			infinite: false,
			lazyLoad: 'ondemand',
			speed: 300,
			slidesToShow: 3,
			// slidesToScroll: 1,
			initialSlide: 0,
			variableWidth: true,
			responsive: [
				{
					breakpoint: 993,
					settings: {
						slidesToShow: 2,

					}
				},
				{
					breakpoint: 769,
					settings: {
						arrows: false
					}
				},
				{
					breakpoint: 576,
					settings: {
						slidesToShow: 1,
						centerMode: true
					}
				}
			]
			// , variableWidth: true
		});
	}
}

$(document).ready(function() {
	ProductSlider.init();
	faqSlider.init();
});
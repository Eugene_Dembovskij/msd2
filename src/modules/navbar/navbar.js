$(function () {
	'use strict';

	window.UI = window.UI || {};
	
	window.UI.navbar = {
		init: function() {
			$(document).on('click', '.js-navbar-toggler', this.click);
		},
		click: function(){
			var header = $(this).parents('.header');
			var navbar = header.find('.navbar');
			var isActive = header.hasClass('is-active');

			if (!isActive) {
				header.addClass('is-active');
				navbar.slideDown(250);
			} else {
				header.removeClass('is-active');
				navbar.slideUp(250);
			}
		}
	};

	window.UI.navbar.init();
}());
'use strict'

$(window).bind("load", function() {
	$('.js-main-section').on('wheel', function(event){
		var mainSlider = MainSlider.el;
		var currentSlide = mainSlider[0].slick.currentSlide;
		var count = mainSlider[0].slick.slideCount - 1;
		var scrollPos = $(window).scrollTop();
		
		if (scrollPos === 0) {
			if (event.originalEvent.deltaY < 0 && currentSlide > 0) {
				mainSlider.slick('slickPrev');
			} else if (event.originalEvent.deltaY > 0) {
				event.preventDefault();
				event.stopPropagation();

				if (currentSlide === count) {
					var nextSection =  $('#anchor-eff');

					$('html, body').stop().animate({
						'scrollTop': nextSection.offset().top
					}, 750);
				}
				mainSlider.slick('slickNext');
			}
		}
	});
});

window.UI = window.UI || {};
	
window.UI.moveToAnchor = {
	init: function() {
		var inst = this;

		$(document).on('click', '.js-anchor', function(e){
			inst.onClick(e);
		});
	},
	onClick: function(e){
		e.preventDefault();
		
		var jThis = $(e.currentTarget);
		var	jTarget = ( jThis.data("target").indexOf(".") == -1 ) ? $("#" + jThis.data("target")) : $(jThis.data("target"));
		
		if( jTarget.length ){
			// if (window.UI.menu.isOpen) {
			// 	window.UI.menu.close();
			// }
			
			$('html, body').stop().animate({
				'scrollTop': jTarget.offset().top
			}, 750);
		}
	}	
};

window.UI.moveToAnchor.init();


window.UI.scrollbarWidth = function() {
	var block = $('<div>').css({'height':'50px','width':'50px'}),
		indicator = $('<div>').css({'height':'200px'});

		$('body').append(block.append(indicator));
		var w1 = $('div', block).innerWidth();
		block.css('overflow-y', 'scroll');
		var w2 = $('div', block).innerWidth();
		$(block).remove();
		return (w1 - w2);
};

var ProductSlider = {
	parentEl: '.js-product-use',
	slider: null,
	sliderOpts: {
		nextArrow: '<span class="slick-arrow--next"></span>',
		prevArrow: '<span class="slick-arrow--prev"></span>',
		infinite: true,
		dots: true,
		arrows: true,
		centerMode: false,
		slidesToShow: 1,
		dotsClass: 'product-use__dots slick-dots',
		// appendArrows: '.js-gerber-pup-slider .rg-b-gerber__pUps-items-nav-box',
		// appendDots: '.js-gerber-pup-slider .rg-b-gerber__pUps-items-dots-box',
		slidesToScroll: 1
	},
	init: function() {
		var oThis = this;
		if ($(oThis.parentEl + ':visible').length) {
			$(oThis.parentEl + ':visible').slick(oThis.sliderOpts);
		}
	}
};

var faqSlider = {
	box: [],
	slider: [],
	init: function () {
		faqSlider.box = $('.js-faq-slider');
		if (faqSlider.box){
			faqSlider.slider = faqSlider.box.find('.js-faq-slider__list');
			if (faqSlider.slider){
				faqSlider.initSlider();
			}
		}
	},
	initSlider: function () {
		faqSlider.slider.slick({
			dots: false,
			infinite: false,
			lazyLoad: 'ondemand',
			speed: 300,
			slidesToShow: 3,
			// slidesToScroll: 1,
			initialSlide: 0,
			variableWidth: true,
			responsive: [
				{
					breakpoint: 993,
					settings: {
						slidesToShow: 2,

					}
				},
				{
					breakpoint: 769,
					settings: {
						arrows: false
					}
				},
				{
					breakpoint: 576,
					settings: {
						slidesToShow: 1,
						centerMode: true
					}
				}
			]
			// , variableWidth: true
		});
	}
}

$(document).ready(function() {
	ProductSlider.init();
	faqSlider.init();
});
$(function () {
	'use strict';

	window.UI = window.UI || {};
	
	window.UI.modal = {
		cTimer: [],
		init: function() {
			var inst = this;

			$(document).on('click', '.js-modal-btn', function(e){
				clearTimeout(window.UI.modal.cTimer);
				window.UI.modal.cTimer = setTimeout(
					function () {
						inst.open(e);
					},250, e);

			});
			$(document).on('click', '.js-modal__close', function(){
				inst.close();
			});
		},
		open: function(e){
			var jThis = $(e.currentTarget);
			var target = jThis.data('target');
			var modal = $('.js-modal');
			var item = modal.find('[data-item=' + target + ']');

			window.UI.modal.onOpenModal(modal, item);
		},
		close: function() {
			$('body').removeClass('overflow');
			$('body').removeClass('is-modal');
			$('body').css({'padding-right': '0'});
			$('.js-modal').removeClass('is-active');
			$('.js-modal__item').hide();
		},
		closeItems: function() { // -закрываем только итемы модалок
			$('.js-modal__item').hide();
		},
		openFromCode: function(idModal){ // открываем программно модалку по её ID
			var target = idModal;
			var modal = $('.js-modal');
			var item = modal.find('[data-item=' + target + ']');

			window.UI.modal.onOpenModal(modal, item);
		},
		onOpenModal: function(modal, item){ // показываем
			if( modal.get(0) && item.get(0)){
				$('body').addClass('overflow');
				$('body').css({'padding-right': window.UI.scrollbarWidth() + 'px'});
				modal.addClass('is-active');
				$('body').addClass('is-modal');
				if(modal.find('.js-modal__item').is(":visible")){
					window.UI.modal.closeItems();
				}
				item.fadeIn(250);
			}
		}
	};

	window.UI.modal.init();
	// window.UI.modal.openFromCode('faq-01');
}());
$(function () {
	'use strict';

	window.UI = window.UI || {};
	
	window.UI.navbar = {
		init: function() {
			$(document).on('click', '.js-navbar-toggler', this.click);
		},
		click: function(){
			var header = $(this).parents('.header');
			var navbar = header.find('.navbar');
			var isActive = header.hasClass('is-active');

			if (!isActive) {
				header.addClass('is-active');
				navbar.slideDown(250);
			} else {
				header.removeClass('is-active');
				navbar.slideUp(250);
			}
		}
	};

	window.UI.navbar.init();
}());
$(function () {
	'use strict';

	window.UI = window.UI || {};
	
	window.UI.quest = {
		rightAnswers: 0,
		init: function() {
			$(document).on('click', '.js-questions-next', this.next);
			$(document).on('click', '.js-questions-choose', this.choose);
			$(document).on('click', '.js-questions-reset', this.reset);
		},
		choose: function(){
			var answer = $(this);
			var help = answer.find('.answer__help');
			var question = answer.parent();
			var rightAnswer = question.find('[data-answer*="true"]');

			if (!question.hasClass('choosen')) {
				help.slideDown(250);
				question.addClass('choosen');
				$('.questions__btn').addClass('active');
				rightAnswer.addClass('lighted');
				
				if (answer.data('answer') == true) {
					window.UI.quest.rightAnswers += 1;
				}
			}
		},
		next: function(){
			var btn = $(this).parent();
			var questionText = $('.questions__title');
			var questionsItem = $('.questions__item');
			var activeQuestion = $('.questions__item.active').index();
			var nextQuestion = questionsItem[activeQuestion + 1];
			var nextQuestionData = $(nextQuestion).data('question');
			var counter = $('.questions__counter');

			if (activeQuestion + 1 <= questionsItem.length) {
				counter.text(activeQuestion + 2 + '/' + questionsItem.length);
				questionsItem.removeClass('active');
				$(nextQuestion).addClass('active');
				questionText.text(nextQuestionData);
				btn.removeClass('active');

				$('html, body').stop().animate({
					'scrollTop': (counter[0].offsetTop)
				}, 750);

				if (activeQuestion + 1 == questionsItem.length - 1) {
					$(this).text('Результаты теста');
				}

				if (activeQuestion + 1 == questionsItem.length) {
					$('.questions').hide();
					$('.test-banner').show();
					console.log(window.UI.quest.rightAnswers);
					$('.js-questions-result').text(window.UI.quest.rightAnswers);
				}
			}
		},
		reset: function() {
			var qItems = $('.questions__item');

			window.UI.quest.rightAnswers = 0;
			$('.test-banner').hide();
			$('.questions').show();
			qItems.removeClass('active choosen');
			$(qItems[0]).addClass('active');
			$('.answer').removeClass('lighted');
			$('.answer__help').hide();

			$('.questions__counter').text('1/' + qItems.length);
		}
	};

	window.UI.quest.init();
}());
$(function () {
	'use strict';

	window.UI = window.UI || {};
	
	window.UI.tooltip = {
		init: function() {
			var inst = this;

			$(document).on('click', '.js-tooltip', function(e){
				inst.open(e);
			});
			$(document).on('click', '.js-tooltip-close', function(){
				inst.close();
			});
		},
		open: function(e){
			var jThis = $(e.currentTarget);
			var parent = jThis.parent();
			var item = parent.find('.js-tooltip-item');

			$('.js-tooltip-item').hide();
			item.slideDown(250);
		},
		close: function() {
			$('.js-tooltip-item').hide();
		}
	};

	window.UI.tooltip.init();
}());